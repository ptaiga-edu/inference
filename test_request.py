# -*- coding: utf-8 -*-
"""Example of request to model inference."""

import json

import click
import pandas as pd
import requests

HOST = '127.0.0.1'  # '192.168.1.54' or '127.0.0.1'  # noqa: SC100
PORT = '8001'
ENDPOINT = 'invocations'  # 'invocations' or 'uploadfile'  # noqa: SC100
DATA_FILE = 'data/data.csv.zip'


@click.command()
@click.argument('host', type=str, default=HOST)
@click.argument('port', type=str, default=PORT)
@click.argument('endpoint', type=str, default=ENDPOINT)
@click.argument('data_file', type=click.Path(), default=DATA_FILE)
def send_request(host: str, port: str, endpoint: str, data_file: str) -> None:
    """Send request with data or file payload."""
    url = f'http://{host}:{port}/{endpoint}'
    print('Request to:', url)
    if endpoint == 'uploadfile':
        files = {'file': open(data_file, 'rb')}
        r = requests.post(url=url, files=files)
    else:
        data = pd.read_csv(data_file).values.tolist()
        headers = {'Content-Type': 'application/json', }
        http_data = json.dumps({'inputs': data})
        r = requests.post(url=url, headers=headers, data=http_data)
    print(f'Received response: {r.text}')
    pred = r.json()['predictions']
    y_pred = [int(p[0][1] > 0.2) for p in pred]
    print(f'Result predictions: {y_pred}')


if __name__ == '__main__':
    send_request()
