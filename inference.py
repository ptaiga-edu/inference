# -*- coding: utf-8 -*-
"""Example of model inference using FastAPI.

Implementation of API close to MLFlow default serving:
`$ mlflow model serve --no-conda -m {s3_model_url} -h 0.0.0.0 -p 8001`

"""

import os

import mlflow
import numpy as np
import pandas as pd
from dotenv import load_dotenv
from fastapi import FastAPI, File, HTTPException, UploadFile
from pydantic import BaseModel

# Load the environment variable from the .env file into the application
load_dotenv()

# Initialize the FastAPI application
app = FastAPI()


# Matching data from POST request
class Item(BaseModel):
    """Data class."""

    inputs: list


# Create a class to store the deployed model and use it for prediction
class Model:
    """Model class."""

    def __init__(self, model_name: str, model_stage: str) -> None:
        """Initialize the model."""
        self.model = mlflow.pyfunc.load_model(
            f'models:/{model_name}/{model_stage}'
        )
        self.name = model_name
        self.stage = model_stage

    def predict(self, data: np.array) -> None:
        """Use loaded model to make predictions."""
        predictions = self.model.predict(data)
        return predictions


# Create model
model = Model(os.getenv('MODEL_NAME'), os.getenv('MODEL_STAGE'))


# Create endpoint for load data
@app.post('/invocations')
async def create_item(item: Item) -> dict:
    """Load data and make prediction."""
    data = np.array(item.inputs)
    predictions = model.predict(data)
    response = {
        'predictions': [p.tolist() for p in predictions],
        'model': {'name': model.name, 'stage': model.stage}
    }
    return response


# Create endpoint for file uploading
@app.post('/uploadfile')
async def create_upload_file(file: UploadFile = File()) -> dict:  # noqa: B008
    """Load file and make prediction."""
    if file.filename.endswith(('.csv', '.tsv', '.csv.zip', '.tsv.zip')):
        # Create a temporary file with the same name
        # as the uploaded file to load the data
        with open(file.filename, 'wb') as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename).values
        os.remove(file.filename)
        predictions = model.predict(data)
        response = {
            'predictions': [p.tolist() for p in predictions],
            'model': {'name': model.name, 'stage': model.stage}
        }
        return response

    else:
        raise HTTPException(
            status_code=400,
            detail='Invalid file format. Only CSV Files accepted.',
        )


# Check if the environment variables for AWS access are available.
# If not, exit the program
if (
    os.getenv('AWS_ACCESS_KEY_ID') is None or
    os.getenv('AWS_SECRET_ACCESS_KEY') is None
):
    exit(1)
