FROM python:3.8

WORKDIR /code

RUN pip install --upgrade pip

COPY ./requirements.txt /code/requirements.txt
COPY ./inference.py /code/inference.py
COPY ./.env /code/.env

RUN pip install -r requirements.txt

CMD ["uvicorn", "inference:app", "--host", "0.0.0.0", "--port", "80"]
