# Model inference

This project is addition to https://gitlab.com/ptaiga-edu/mlops.
It performs function of remote MLFlow-server for tracking model and using them in production purposes.


## Build image for remote deployment:
 - `Dockerfile`
 - `requirements.txt`
 - `.env-example` (rename to `.env`)
 - `inference.py`

Now `docker-compose.yaml` and other artifacts for building necessary images are placed here: https://gitlab.com/ptaiga-edu/mlops/-/tree/main/docker


## Local deployment:
`$ uvicorn inference:app --host 0.0.0.0 --port 8001`


## Test requests:
 - `requirements-dev.txt`
 - `data/data.csv.zip` - test sentence was converted by `TfIdfVectorizer` and saved via `pandas.DataFrame.to_csv`
 - `$ python test_request.py` or
   `$ python test_request.py 127.0.0.1 8001 invocations data/data.csv.zip` - send request for testing inference server.
